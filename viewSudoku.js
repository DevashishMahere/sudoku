function checkInput() {
	//console.log("check Input");
	
	var check=0;	
	for(i=0;i<9;i+=3)
	{
		for(j=0;j<9;j+=3)
		{					
			for(n=i;n<i+3;n++)
			{	
				for(m=j;m<j+3;m++)
				{
					if(initialSudoku[n][m]==0)
					{
						var str=check+"Id";
						check++;
						var toBeChecked=document.getElementById(str).value;
						//console.log(toBeChecked);
						if(isNaN(toBeChecked))
						{
							if(typeof(toBeChecked)!="number")
							{
								alert("please enter number between 1-9");
								document.getElementById(str).value="";
							}							
						}
						else
						{
							var a=parseInt(document.getElementById(str).value);
							if(typeof(a)=="number"&&a==0)
							{
								alert("please enter number between 1-9");
								document.getElementById(str).value="";
							}
						}	
					}	
				}
			}							
		}
	}
	
}

function showSudoku() {
	//console.log("check");

		var mainTable=document.createElement("table");
		mainTable.setAttribute("id","mainTable");

		for(i=0;i<9;i+=3)
		{
			var mainRow=document.createElement("tr");
			var str="mainRow"+i;
			mainRow.setAttribute("id",str);

			for(j=0;j<9;j+=3)
			{
				var mainCol=document.createElement("td");
				str="mainCol"+i+""+j;
				mainCol.setAttribute("id",str);

				var subTable=document.createElement("table");
				str="table"+i+""+j;
				subTable.setAttribute("id",str);					
				for(n=i;n<i+3;n++)
				{	
					var subRow=document.createElement("tr");
					str="subRow"+n+""+j;
					for(m=j;m<j+3;m++)
					{
						var subCol=document.createElement("td");
						str="subCol"+m+""+j;
						if(initialSudoku[n][m]==0)
						{
							var str=setId+"Id";
							var input=document.createElement("input");
							input.classList.add("form-control");
							input.setAttribute("id",str);
							input.setAttribute("type","text");
							input.setAttribute("maxlength","1");
							input.setAttribute("oninput","checkInput()");
							setId++;
							// input.setAttribute("type","number");
							// input.setAttribute("min","1");
							// input.setAttribute("max","9");
							subCol.appendChild(input);
							input.setAttribute("style","width: 40px;");
						}
						else
						{
							var input=document.createElement("input");
							input.classList.add("form-control");
							input.setAttribute("style","width: 40px;");
							input.setAttribute("disabled",true);
							var str=initialSudoku[n][m];
							input.setAttribute("placeholder",str);
							subCol.appendChild(input);	
						}
						subRow.appendChild(subCol);
					}
					subTable.appendChild(subRow);
				}					
				mainCol.appendChild(subTable);
				mainRow.appendChild(mainCol);		
			}
			mainTable.appendChild(mainRow);
		}

		var element=document.getElementById("mainDiv");
		element.appendChild(mainTable);
		
		document.getElementById("mainDiv").style.visibility='visible';
		document.getElementById("submitDiv").style.visibility='visible';
			var h=0,m=0,s=0;
			setIntervalId=setInterval(function(){
				s++;
				if(s==61)
				{
					m++;
					s=0;
					if(m==61)
					{
						h++;
						m=0;
					}
				}
				var str="Timer "+h+":"+m+":"+s;
				document.getElementById("timer").innerHTML=str;
		},1000);
}

function showSudokuOnlyTable(checkSudoku){
	var mainTable=document.createElement("table");
	mainTable.setAttribute("id","mainTable");

	for(i=0;i<9;i+=3)
	{
		var mainRow=document.createElement("tr");
		var str="mainRow"+i;
		mainRow.setAttribute("id",str);

		for(j=0;j<9;j+=3)
		{
			var mainCol=document.createElement("td");
			str="mainCol"+i+""+j;
			mainCol.setAttribute("id",str);

			var subTable=document.createElement("table");
			str="table"+i+""+j;
			subTable.setAttribute("id",str);					
			for(n=i;n<i+3;n++)
			{	
				var subRow=document.createElement("tr");
				str="subRow"+n+""+j;
				for(m=j;m<j+3;m++)
				{
					var subCol=document.createElement("td");
					str="subCol"+m+""+j;
					if(initialSudoku[n][m]==0)
					{
						var str=setId+"Id";
						var input=document.createElement("input");
						input.classList.add("form-control");
						input.setAttribute("id",str);
						input.setAttribute("type","text");
						input.setAttribute("maxlength","1");
						input.setAttribute("oninput","checkInput()");
						setId++;
						//input.setAttribute("min","1");
						//input.setAttribute("max","9");
						if(!isNaN(checkSudoku[n][m])&&checkSudoku[n][m]!=0)
						{
							str=checkSudoku[n][m];
							input.setAttribute("value",str);
						}
						subCol.appendChild(input);
						input.setAttribute("style","width: 40px;");
					}
					else
					{
						var input=document.createElement("input");
						input.classList.add("form-control");
						input.setAttribute("style","width: 40px;");
						input.setAttribute("disabled",true);
						var str=initialSudoku[n][m];
						input.setAttribute("placeholder",str);
						subCol.appendChild(input);	
					}
					subRow.appendChild(subCol);
				}
				subTable.appendChild(subRow);
			}					
			mainCol.appendChild(subTable);
			mainRow.appendChild(mainCol);		
		}
		mainTable.appendChild(mainRow);
	}

	var element=document.getElementById("mainDiv");
	element.appendChild(mainTable);
}

