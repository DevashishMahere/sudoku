window.onload=function(){
	var submitButton=document.getElementById("submitButton");
	submitButton.addEventListener("click",onSubmit);
	
	function onSubmit() {
	var check=0;
	var checkSudoku=[];
	for(i=0;i<9;i++)
	{
		checkSudoku[i]=[];
		for(j=0;j<9;j++)
		{	
			checkSudoku[i][j]=initialSudoku[i][j];
		}
	}
	for(i=0;i<9;i+=3)
	{
		for(j=0;j<9;j+=3)
		{					
			for(n=i;n<i+3;n++)
			{	
				for(m=j;m<j+3;m++)
				{
					if(initialSudoku[n][m]==0)
					{
						var str=check+"Id";
						check++;
						checkSudoku[n][m]=parseInt(document.getElementById(str).value);						
					}	
				}
			}							
		}
	}

	// for(i=0;i<9;i++)
	// {	
	// 	var str=" "
	// 	for(j=0;j<9;j++)
	// 	{
	// 		str+=checkSudoku[i][j];
	// 		str+=" ";
	// 	}
	// 	console.log(str);
	// }

	var flag=0;
	for(i=0;i<9;i++)
	{
		var arr1to9=[];
		for(var loop=0;loop<9;loop++)
			arr1to9[loop]=loop+1;

		for(j=0;j<9;j++)
		{
			k=checkSudoku[i][j];
			l=arr1to9.indexOf(k);
			if(l==-1)
			{
				flag=1;
				break;	
			}
			else
			{
				arr1to9.splice(l,1);
			}
		}
		if(arr1to9.length>0)
			flag=1;
		if(flag==1)
			break;
	}

	//console.log(flag);
	if(flag==0)
	{
		for(i=0;i<9;i++)
		{
			var arr1to9=[];
			for(var loop=0;loop<9;loop++)
				arr1to9[loop]=loop+1;

			for(j=0;j<9;j++)
			{
				k=checkSudoku[j][i];
				l=arr1to9.indexOf(k);
				if(l==-1)
				{
					flag=1;
					break;	
				}
				else
				{
					arr1to9.splice(l,1);
				}
			}
			if(arr1to9.length>0)
				flag=1;
			if(flag==1)
				break;
		}
	}

	//console.log(flag);

	if(flag==0)
	{
		for(i=0;i<9;i+=3)
		{
			for(j=0;j<9;j+=3)
			{
				var arr1to9=[];
				for(var loop=0;loop<9;loop++)
					arr1to9[loop]=loop+1;
				for(n=i;n<i+3;n++)
				{
					for(m=j;m<j+3;m++)
					{
						k=checkSudoku[n][m];
						l=arr1to9.indexOf(k);
						if(l==-1)
						{
							flag=1;
							break;	
						}
						else
						{
							arr1to9.splice(l,1);
						}
					}
					if(flag==1)
						break;
				}

				if(arr1to9.length>0)
					flag=1;

				if(flag==1)
					break;			
			}
			if(flag==1)
			break;
		}
	}
	//console.log(flag);
	if(flag==0)
	{
		document.getElementById("submitDiv").style.visibility='hidden';
		document.getElementById("mainDiv").style.visibility='hidden';
	
		alert('well done you have solved sudoku');

		if(document.getElementById("easyButton").classList.contains("btn-success"))
		{
			document.getElementById("easyButton").classList.remove("btn-success");
			document.getElementById("easyButton").classList.add("btn-primary")	
		}
		if(document.getElementById("mediumButton").classList.contains("btn-success"))
		{
			document.getElementById("mediumButton").classList.remove("btn-success");
			document.getElementById("mediumButton").classList.add("btn-primary")	
		}
		if(document.getElementById("hardButton").classList.contains("btn-success"))
		{
			document.getElementById("hardButton").classList.remove("btn-success");
			document.getElementById("hardButton").classList.add("btn-primary")	
		}
	}
	else
	{
		var rem=document.getElementById("mainTable");
		rem.remove();
		setId=0;
		showSudokuOnlyTable(checkSudoku);
		alert('wrong submission try again');
	}

	}
}