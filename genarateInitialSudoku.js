function randomSolvedSudoku(){
	
	if(setId!=0)
	{
		setId=0;
		var rem=document.getElementById("mainTable");
		rem.remove();
		clearInterval(setIntervalId);
	}

	var i,j,k,l;
	for(i=0;i<9;i++)
	{
		solvedSudoku[i]=[];
		for(j=0;j<9;j++)
			solvedSudoku[i][j]=0;
	}

	var arr1to9=[];
	for(i=0;i<9;i++)
	arr1to9[i]=i+1;

	for(i=0;i<9;i++)
	{
		l=arr1to9.length;
		k=Math.floor((Math.random()*l)+0);
		k=arr1to9[k]
		solvedSudoku[0][i]=k;
		j=arr1to9.indexOf(k);
		arr1to9.splice(j,1);
	}

	var flag=0;
	while(1)
	{
		var loopFlag=0;
		for(i=1;i<9;i++)
		{
			for(j=0;j<9;j++)
			{
				var arr1to9=[];
				for(var loop=0;loop<9;loop++)
				arr1to9[loop]=loop+1;

				for(var inRow=0;inRow<i;inRow++)
				{
					k=solvedSudoku[inRow][j];
					l=arr1to9.indexOf(k);
					if(l!=-1)
					arr1to9.splice(l,1);
				}

				for(var inCol=0;inCol<j;inCol++)
				{
					k=solvedSudoku[i][inCol];
					l=arr1to9.indexOf(k);
					if(l!=-1)
					arr1to9.splice(l,1);
				}

				var rowStart=Math.floor(i/3)*3;
				var colStart=Math.floor(j/3)*3;
				var colEnd=colStart+3;
				
				for(var inRow=rowStart;inRow<=i;inRow++)
				{
					if(inRow<i)
					{
						for(var inCol=colStart;inCol<colEnd;inCol++)
						{
							k=solvedSudoku[inRow][inCol];
							l=arr1to9.indexOf(k);
							if(l!=-1)
							arr1to9.splice(l,1);
						}
					}
					else
					{
						for(var inCol=colStart;inCol<j;inCol++)
						{
							k=solvedSudoku[inRow][inCol];
							l=arr1to9.indexOf(k);
							if(l!=-1)
							arr1to9.splice(l,1);
						}				
					}
				}

				if(!arr1to9.length)
				{
					loopFlag=1;
					break;
				}
				while(1)
				{
					l=arr1to9.length;
					k=Math.floor((Math.random()*l)+0);
					k=arr1to9[k];
					solvedSudoku[i][j]=k;
					var toRemove=arr1to9.indexOf(k);
					if(toRemove!=-1)
					{
						arr1to9.splice(toRemove,1);
						break;
					}
				}
			}
			if(loopFlag)
			break;
		}
		if(i==9)
		break;
	}

	// for(i=0;i<9;i++)
	// {	
	// 	var str=" "
	// 	for(j=0;j<9;j++)
	// 	{
	// 		str+=solvedSudoku[i][j];
	// 		str+=" ";
	// 	}
	// 	console.log(str);
	// }

	for(i=0;i<9;i++)
	{	
		initialSudoku[i]=[]
		for(j=0;j<9;j++)
			initialSudoku[i][j]=0;
	}
}


function startGameEasy(){
	if(document.getElementById("easyButton").classList.contains("btn-primary"))
	{
		document.getElementById("easyButton").classList.remove("btn-primary");
		document.getElementById("easyButton").classList.add("btn-success")	
	}
	if(document.getElementById("mediumButton").classList.contains("btn-success"))
	{
		document.getElementById("mediumButton").classList.remove("btn-success");
		document.getElementById("mediumButton").classList.add("btn-primary")	
	}
	if(document.getElementById("hardButton").classList.contains("btn-success"))
	{
		document.getElementById("hardButton").classList.remove("btn-success");
		document.getElementById("hardButton").classList.add("btn-primary")	
	}	
	randomSolvedSudoku();
	i=Math.floor((Math.random()*6)+0);
	k=31+i;
	while(k--)
	{
		while(1)
		{
			i=Math.floor((Math.random()*9)+0);
			j=Math.floor((Math.random()*9)+0);
			if(initialSudoku[i][j]==0)
			{
				initialSudoku[i][j]=solvedSudoku[i][j];
				break;
			}
		}
	}
	showSudoku();
}
function startGameMedium(){

	if(document.getElementById("easyButton").classList.contains("btn-success"))
	{
		document.getElementById("easyButton").classList.remove("btn-success");
		document.getElementById("easyButton").classList.add("btn-primary")	
	}
	if(document.getElementById("mediumButton").classList.contains("btn-primary"))
	{
		document.getElementById("mediumButton").classList.remove("btn-primary");
		document.getElementById("mediumButton").classList.add("btn-success")	
	}
	if(document.getElementById("hardButton").classList.contains("btn-success"))
	{
		document.getElementById("hardButton").classList.remove("btn-success");
		document.getElementById("hardButton").classList.add("btn-primary")	
	}

	randomSolvedSudoku();
	i=Math.floor((Math.random()*4)+0);
	k=27+i;
	while(k--)
	{
		while(1)
		{
			i=Math.floor((Math.random()*9)+0);
			j=Math.floor((Math.random()*9)+0);
			if(initialSudoku[i][j]==0)
			{
				initialSudoku[i][j]=solvedSudoku[i][j];
				break;
			}
		}
	}
	showSudoku();
}
function startGameHard(){

	if(document.getElementById("easyButton").classList.contains("btn-success"))
	{
		document.getElementById("easyButton").classList.remove("btn-success");
		document.getElementById("easyButton").classList.add("btn-primary")	
	}
	if(document.getElementById("mediumButton").classList.contains("btn-success"))
	{
		document.getElementById("mediumButton").classList.remove("btn-success");
		document.getElementById("mediumButton").classList.add("btn-primary")	
	}
	if(document.getElementById("hardButton").classList.contains("btn-primary"))
	{
		document.getElementById("hardButton").classList.remove("btn-primary");
		document.getElementById("hardButton").classList.add("btn-success")	
	}

	randomSolvedSudoku();
	i=Math.floor((Math.random()*5)+0);
	k=22+i;
	while(k--)
	{
		while(1)
		{
			i=Math.floor((Math.random()*9)+0);
			j=Math.floor((Math.random()*9)+0);
			if(initialSudoku[i][j]==0)
			{
				initialSudoku[i][j]=solvedSudoku[i][j];
				break;
			}
		}
	}
	showSudoku();
}